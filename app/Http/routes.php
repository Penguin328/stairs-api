<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/exercise', function() {
    return \Stairs\Exercise::all();
});

$app->get('/user', function() {
    return \Stairs\User::all();
});

$app->get('/user/{id}', function($id) {
    return \Stairs\User::find($id)->with('exercises')->get();
});
